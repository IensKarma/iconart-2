ARTWEBIT project: IconArt
=========================
#### PHP: 5.5.9+
#### CMS: Opencart 2.*
#### Database: MySQL 5.5.41+

INSTALLATION
------------
#### Clone project
~~~
git clone https://{your account on bitbucket}@bitbucket.org/psd2html_/iconart.git
~~~
#### Configure Config
~~~
.../iconart/shop/config/config.php
and
.../iconart/shop/admin/config/config.php
~~~