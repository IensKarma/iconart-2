<?php
// Enables PHP error reporting
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Configuration
if(file_exists('config/local/config.php') && is_file('config/local/config.php')){
    require_once('config/local/config.php');
}else{
    require_once('config/config.php');
}

////testchange
//// Install
//if (!defined('DIR_APPLICATION')) {
//	header('Location: install/index.php');
//	exit;
//}

// VirtualQMOD
require_once('./vqmod/vqmod.php');
VQMod::bootup();

// VQMODDED Startup
require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

start('catalog');