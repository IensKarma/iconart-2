<?php
// Text
$_['text_title']       = 'Payment by Bank Card (VISA/MasterCard)'.'<div><img src=http://'.$_SERVER['SERVER_NAME'].'/catalog/view/theme/default/image/payment/avangard.png></div>';
$_['text_instruction'] = 'Instructions for payment cart through the bank.';
$_['text_description'] = 'Please follow the system prompts.';
$_['text_payment']     = 'The order will not be processed if the bank has rejected the transaction on the card.';
