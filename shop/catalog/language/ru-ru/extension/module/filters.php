<?php
// Heading
$_['heading_title'] = 'Фильтр товаров';
$_['price_label'] = 'Цена';
$_['show_label'] = 'Показать';
$_['stock_label'] = 'Есть в наличии';
$_['manufacturers_label'] = 'По производителю';