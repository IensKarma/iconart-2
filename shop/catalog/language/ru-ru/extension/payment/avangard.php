<?php
// Text
$_['text_title']       = 'Оплата Банковской Картой (VISA/MasterCard)'.'<div><img src=http://'.$_SERVER['SERVER_NAME'].'/catalog/view/theme/default/image/payment/avangard.png></div>';
$_['text_instruction'] = 'Инструкции по оплате картой через банк';
$_['text_description'] = 'Пожалуйста, следуйте подсказкам системы.';
$_['text_payment']     = 'Заказ не будет обработан, если банк отклонил транзакцию по карте.';
