<form id=mmmid action="<?= $action; ?>" method="post" accept-charset="UTF-8">
    <input type="hidden" name="avangard_shop_id" value="<?= $avangard_shop_id; ?>" />
    <input type="hidden" name="avangard_shop_sign" value="<?= $avangard_shop_sign; ?>" />
    <input type="hidden" name="avangard_pwd" value="<?= $avangard_pwd; ?>" />

    <input type="hidden" name="avangard_order_id" value="<?= $avangard_order_id; ?>" />
    <input type="hidden" name="avangard_order_total"  value="<?= $avangard_order_total; ?>" />

    <input type="hidden" name="ap_returnurl" value="<?= $ap_returnurl; ?>" />
    <input type="hidden" name="ap_cancelurl" value="<?= $ap_cancelurl; ?>" />

    <input type="hidden" name="client_name" value="<?= ControllerExtensionPaymentAvangard::translateFromCyrillicToLatin($client_name); ?>" />
    <input type="hidden" name="client_address" value="<?= ControllerExtensionPaymentAvangard::translateFromCyrillicToLatin($client_address); ?>" />
    <input type="hidden" name="client_email" value="<?= ControllerExtensionPaymentAvangard::translateFromCyrillicToLatin($client_email); ?>" />
    <input type="hidden" name="client_phone" value="<?= ControllerExtensionPaymentAvangard::translateFromCyrillicToLatin($client_phone); ?>" />
</form>

<h2><?= $text_instruction; ?></h2>
<div class="content">
    <p><?= $text_description; ?></p>
    <p><?= $text_payment; ?></p>
</div>
<div class="text-right">
    <input type="checkbox"  onchange="document.getElementById('button-confirm').disabled = !this.checked;" />
    <label class="label-offer"><p>Я согласен с договором <a href="<?= $avangard_offer_url; ?>" target="popup" onclick="window.open('<?= $avangard_offer_url; ?>','popup','width=600,height=600'); return false;">оферты</a></p></label>
    <input type="submit" id='button-confirm' value="<?= $button_confirm; ?>" class="btn btn-primary" disabled />
</div>

<script type="text/javascript"><!--
    $('#button-confirm').bind('click', function() {
        $.ajax({
            type: 'get',
            url: 'index.php?route=extension/payment/avangard/request',
            success: function() {
                $('#checkout').submit();
                location = '<?= $continue; ?>';
                document.getElementById('mmmid').submit();
            }
        });
    });
    //--></script>
<style>
    .btn-danger {
        position: absolute !important;
        margin-top: -33px !important;
    }
    input[type=checkbox]{
        margin: 4px 5px 0 0;
    }
    .label-offer{
        margin: 4px 20px 0 0;
    }

</style>



