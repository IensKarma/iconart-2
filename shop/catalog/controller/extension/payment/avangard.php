<?php

/**
 * Class ControllerExtensionPaymentAvangard
 *
 * @version 1.0
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 * @copyright (c) 2017-2018 ARTWEBIT Team <it@artwebit.ru>
 */
class ControllerExtensionPaymentAvangard extends Controller {
    // constants order status
    const ORDER_NOT_FOUND = 0;
    const PROCESSED       = 1;
    const REJECTED        = 2;
    const ACCOMPLISHED    = 3;
    const PARTIAL_RETURN  = 5;
    const REFUND          = 6;

    /**
     * This method index
     */
    public function index() {
        $this->language->load('extension/payment/avangard');// load language file

        // Set text to variables
        $data['text_instruction'] = $this->language->get('text_instruction');// text_instruction
        $data['text_description'] = $this->language->get('text_description');// text_description
        $data['text_payment']     = $this->language->get('text_payment');// text_payment

        // Set variable from session
        $data['avangard_order_id'] = $this->session->data['order_id'];// avangard_order_id

        /** @var float $order_info */
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        // Set data to variables
        $data['avangard_order_total'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);// avangard_order_total
        $data['avangard_shop_id']     = $this->config->get('avangard_shop_id');// avangard_shop_id
        $data['avangard_shop_sign']   = $this->config->get('avangard_shop_sign');// avangard_shop_sign
        $data['avangard_pwd']         = $this->config->get('avangard_pwd');// avangard_pwd
        $data['avangard_offer_url']   = $this->config->get('avangard_offer_url');// avangard_offer_url
        $data['client_name']          = $order_info['firstname'] . ' ' . $order_info['lastname'];//client_name
        $data['client_address']       = $order_info['payment_address_1'];
        $data['client_email']         = $order_info['email'];
        $data['client_phone']         = $order_info['telephone'];

        // Set data to session
        $this->session->data['avangard_shop_id']     = $this->config->get('avangard_shop_id');
        $this->session->data['avangard_shop_sign']   = $this->config->get('avangard_shop_sign');
        $this->session->data['avangard_pwd']         = $this->config->get('avangard_pwd');
        $this->session->data['avangard_order_total'] = $data['avangard_order_total'];

        //Set user data to session
        $this->session->data['client_name']    = $order_info['firstname'] . ' ' . $order_info['lastname'];
        $this->session->data['client_address'] = $order_info['payment_address_1'];
        $this->session->data['client_email']   = $order_info['email'];
        $this->session->data['client_phone']   = $order_info['telephone'];

        $data['action']         = $this->url->link('extension/payment/avangard/request', '', true);// Action
        $data['ap_returnurl']   = $this->url->link('checkout/success', true);// Return url
        $data['ap_cancelurl']   = $this->url->link('checkout/checkout', '', true);// Cancel url
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['continue']       = $this->url->link('checkout/success');

        return $this->load->view('extension/payment/avangard', $data);
    }

    /**
     * This method confirm
     */
    public function confirm() {
        // return header
        header("HTTP/1.1 202 Accepted");
        /** @var string $localSuccessUrl */
        $localSuccessUrl = $this->url->link('checkout/success', true);

        $this->load->model('checkout/order');//load model

        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('avangard_order_status_id'), "Confirm #{$this->session->data['order_id']}", true);

        // Redirect to success
        $this->response->redirect($localSuccessUrl);
    }

    /**
     * This function get request from bank if payment success or rejected
     * @return void
     */
    public function callback(){
        /** @var array $resInfo */
        $resInfo = $_POST;
        /** @var string $raw */
        $raw = file_get_contents('php://input');

        if(isset($resInfo['order_info']) && $raw == ''){// Check if isset $_POST and raw in post request = ''
            $this->postQueryProcessor($resInfo);//call method
        }elseif ($raw != ''){// Check if have raw in post request and raw != ''
            $this->xmlQueryProcessor($raw);//call method
        }else{// If else return 403 Rejected
            header("HTTP/1.1 403 Rejected");
        }
    }

    /**
     * Processed Post
     * @param array $resInfo
     * @return void
     */
    private function postQueryProcessor($resInfo){
        /** @var string $dbPrefix */
        $dbPrefix = DB_PREFIX;

        /** @var array $orderInfo */
        $orderInfo = $this->db->query("SELECT * FROM {$dbPrefix}order oo WHERE oo.order_id='{$resInfo['order_info']['order_number']}'")->rows[0];

        /** @var int $statusCode */
        $statusCode = $resInfo['order_info']['status_code'];

        // Check if status code = rejected '2'
        if($statusCode == ControllerExtensionPaymentAvangard::REJECTED || $resInfo['order_info']['signature'] == $orderInfo['avangard_signature']){
            /** @var int $orderStatus */
            $orderStatus = 8; //Denied

            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($resInfo['order_info']['order_number'], $orderStatus, 'Denied by bank', true);
        }
        // Check if status code = accomplished '3'
        if($statusCode == ControllerExtensionPaymentAvangard::ACCOMPLISHED || $resInfo['order_info']['signature'] == $orderInfo['avangard_signature']){
            /** @var int $orderStatus status code from settings */
            $orderStatus= $this->config->get('avangard_order_status_id');

            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($resInfo['order_info']['order_number'], $orderStatus, 'Accomplished by bank', true);

            header("HTTP/1.1 202 Accepted");
        }
        header("HTTP/1.1 202 Accepted");
    }

    /**
     * Processed XML
     * @param string $raw
     * @return void
     */
    private function xmlQueryProcessor($raw){
        /** @var string $dbPrefix */
        $dbPrefix = DB_PREFIX;

        /** @var string $pos */
        $pos = strpos($raw, '<');
        /** @var string $res */
        $raw=substr($raw,$pos);
        /** @var object $xml */
        $xml = new SimpleXMLElement($raw);

        /** @var int $statusCode */
        $statusCode = (int)$xml->status_code;
        /** @var int $orderId */
        $orderId = (int)$xml->order_number;

        /** @var array $orderInfo */
        $orderInfo = $this->db->query("SELECT * FROM {$dbPrefix}order oo WHERE oo.order_id='{$orderId}'")->rows[0];

        if($statusCode == ControllerExtensionPaymentAvangard::REJECTED || $xml->signature == $orderInfo['avangard_signature']){// Check if status code = rejected '2'
            /** @var int $orderStatus */
            $orderStatus = 8; //Denied

            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($orderId, $orderStatus, 'Denied by bank', true);
        }
        if($statusCode == ControllerExtensionPaymentAvangard::ACCOMPLISHED || $xml->signature == $orderInfo['avangard_signature']){// Check if status code = accomplished '3'
            /** @var int $statusCode status code from settings */
            $orderStatus = $this->config->get('avangard_order_status_id');

            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($orderId, $orderStatus, 'Accomplished by bank', true);

            header("HTTP/1.1 202 Accepted");
        }
        header("HTTP/1.1 202 Accepted");
    }

    /**
     * This method do first request to bank
     */
    public function request(){
        /** @var string $urlForCurl */
        $urlForCurl =  $this->config->get('avangard_url_reg');
        /** @var string $paymentUrl */
        $paymentUrl = $this->config->get('avangard_url_pay');
        /** @var float $orderTotal */
        $orderTotal =  $this->session->data['avangard_order_total'];
        /** @var array $order */
        $order=explode('.',$orderTotal);
        /** @var float $orderTotal */
        $orderTotal=$order[0];
        /** @var float $orderTotalF */
        $orderTotalF=$orderTotal*100;
        /** @var string $ticket xml*/
        $ticket='<?xml version="1.0" encoding="UTF-8"?>
                    <NEW_ORDER>
                        <SHOP_ID>' . $this->config->get('avangard_shop_id') . '</SHOP_ID>
                        <SHOP_PASSWD>' . $this->config->get('avangard_pwd') . '</SHOP_PASSWD>
                        <AMOUNT>' . $orderTotalF . '</AMOUNT>
                        <ORDER_NUMBER>' . $this->session->data['order_id'] . '</ORDER_NUMBER>
                        <ORDER_DESCRIPTION>' . $this->session->data['order_id'] . ' = ' . $orderTotal . '</ORDER_DESCRIPTION>
                        <LANGUAGE>' . 'RU' . '</LANGUAGE>    
                        <BACK_URL>' . $this->url->link('extension/payment/avangard/rerequest') . '</BACK_URL>
                        <CLIENT_NAME>' . self::translateFromCyrillicToLatin($this->session->data['client_name']) . '</CLIENT_NAME>
                        <CLIENT_ADDRESS>' . self::translateFromCyrillicToLatin($this->session->data['client_address']) . '</CLIENT_ADDRESS>
                        <CLIENT_EMAIL>' . self::translateFromCyrillicToLatin($this->session->data['client_email']) . '</CLIENT_EMAIL>
                        <CLIENT_PHONE>' . self::translateFromCyrillicToLatin($this->session->data['client_phone']) . '</CLIENT_PHONE>
                        <CLIENT_IP>' . $_SERVER['SERVER_ADDR'] . '</CLIENT_IP>
                    </NEW_ORDER>';

        /** @var mixed $curl. Initialize a cURL session */
        $curl = curl_init($urlForCurl);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'xml='.$ticket);
        /** @var mixed $res. Perform a cURL session */
        $result = curl_exec($curl);


        if(!$result){// check if error
            /** @var string $error */
            $error = curl_error($curl).'('.curl_errno($curl).')';
            echo $error;
        }else{// if not error, then print the result
            /** @var string $pos */
            $pos = strpos($result, '<');
            /** @var string $res */
            $result=substr($result,$pos);
            /** @var  $xml */
            $xml = new SimpleXMLElement($result);

            /** @var string $ticket */
            $ticket= (string)$xml->ticket;
            /** @var int $avOrderId */
            $avOrderId = (string)$xml->id;
            /** @var string $okCode */
            $okCode = (string)$xml->ok_code;
            /** @var string $failureCode */
            $failureCode =(string)$xml->failure_code;

            // Set variable to session
            $_SESSION['order_id'] = $this->session->data['order_id'];

            /** @var string $signature */
            $signature = strtoupper(md5(strtoupper(md5($this->config->get('avangard_av_sign')).md5($this->config->get('avangard_shop_id').$this->session->data['order_id'].$orderTotalF))));
            /** insert */
            $this->db->query("UPDATE oc_order oo SET oo.avangard_order_id = '{$avOrderId}', oo.avangard_failure_code = '{$failureCode}', oo.avangard_ok_code = '{$okCode}',oo.avangard_ticket = '{$ticket}', oo.avangard_signature = '{$signature}', oo.avangard_amount ='{$orderTotalF}' WHERE oo.order_id = '{$this->session->data['order_id']}'");

            // Auto form sent
            print "<form id=senttopay method=post action='$paymentUrl' accept-charset='UTF-8'>
                       <input type=hidden name=ticket value='$ticket'>
                   </form>
                    <script>
                        document.getElementById('senttopay').submit();
                    </script>
                  ";
        }
        /** Close a cURL session */
        curl_close($curl);
    }

    /**
     * This method do repeated request to bank
     */
    public function rerequest(){
        /** @var string $dbPrefix */
        $dbPrefix = DB_PREFIX;
        /** @var array $orderInfo */
        $orderInfo = $this->db->query("SELECT * FROM {$dbPrefix}order oo WHERE oo.order_id='{$_SESSION['order_id']}'")->rows[0];

        /** @var string $urlForCurl */
        $urlForCurl = $this->config->get('avangard_url_get_order_info');
        /** @var mixed $ticket */
        $ticket = $orderInfo['avangard_ticket'];
        /** @var int $orderId */
        $orderId = $orderInfo['order_id'];

        /** @var string $localBadUrl */
        $localBadUrl = $this->url->link('checkout/checkout');
        /** @var string $localSuccessUrl */
        $localSuccessUrl = $this->url->link('checkout/success', true);

        /** @var string $tt  xml */
        $tt='<?xml version="1.0" encoding="UTF-8"?>
                <get_order_info>
                    <ticket>' . $ticket . '</ticket>
                    <shop_id>' . $this->config->get('avangard_shop_id') . '</shop_id>
                    <shop_passwd>' . $this->config->get('avangard_pwd') . '</shop_passwd>
                </get_order_info>
        ';
        /** @var mixed $curl. Initialize a cURL session */
        $curl = curl_init($urlForCurl);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'xml='.$tt);
        /** @var mixed $res. Perform a cURL session */
        $result = curl_exec($curl);
        $status = false;

        if(!$result){// check if error, then get number and message
            /** @var string $error */
            $error = curl_error($curl).'('.curl_errno($curl).')';
            echo $error;
        }else{// if not error, then print the result
            /** print $result. For debug uncomment */
//            echo $result;
//             echo '<br>';
//             echo '<br>';
//             echo '<br>';
//            echo var_dump($order_info);
//            die;
            /** Close a cURL session */
            curl_close($curl);

            /** @var  string $pos */
            $pos = strpos($result, '<');
            /** @var string $res */
            $result=substr($result,$pos);
            /** @var  $xml */
            $xml = new SimpleXMLElement($result);
            /** @var  $status */
            $status=$xml->status_code;

            if ($status == ControllerExtensionPaymentAvangard::ACCOMPLISHED) {
                // Clear session
                $_SESSION['cart'] = [];
                print "<script>alert('Оплата прошла успешно.');
	                    document.location.href='http://{$_SERVER['SERVER_NAME']}/index.php?route=extension/payment/avangard/confirm'
                    </script>";
            } else {
                print "<script>alert('Оплата не проведена: Отказ банка – эмитента карты. Ошибка в процессе оплаты, указаны неверные данные карты.');
	                    document.location.href='{$localBadUrl}'
                    </script>";
            }
        }
    }

    /**
     * This function replace cyrillic to latin
     * @param string $string cyrillic
     *
     * @return string latin
     */
    public static function translateFromCyrillicToLatin($string){
        /** @var array $charList */
        $charList = array(
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ж' => 'J', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L',
            'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P',
            'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' =>'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'Ts', 'Ч' => 'Ch',
            'Ш' => 'Sh', 'Щ' => 'Sch', 'Ъ' => '`', 'Ы' => 'Yi',
            'Ь' => '\'', 'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
            'д' => 'd', 'е' => 'e', 'ж' => 'j', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p',
            'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => 'y', 'ы' => 'yi',
            'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        );
        return strtr($string,$charList);
    }

}
