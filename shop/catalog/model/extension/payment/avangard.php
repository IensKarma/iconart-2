<?php

/**
 * Class ModelExtensionPaymentAvangard
 *
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 */
class ModelExtensionPaymentAvangard extends Model {
    /**
     * @inheritdoc
     */
    public function getMethod($address, $total) {
        // Load language file
        $this->load->language('extension/payment/avangard');
        /** @var object $query */
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('avangard_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if ($this->config->get('avangard_total') > $total) {
            $status = false;
        } elseif (!$this->config->get('avangard_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }
        /** @var array $method_data */
        $method_data = [];

        if ($status) {
            $method_data = [
                'code'       => 'avangard',
                'title'      => $this->language->get('text_title'),
                'terms'      => '',
                'sort_order' => $this->config->get('avangard_sort_order')
            ];
        }

        return (array)$method_data;
    }
}
