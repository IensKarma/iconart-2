<?= $header; ?><?= $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-bank-transfer" data-toggle="tooltip" title="<?= $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?= $cancel; ?>" data-toggle="tooltip" title="<?= $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?= $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?= $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= $text_edit; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form-avangard-hosted" class="form-horizontal">
                        <ul class="nav nav-tabs">
                            <li class="active" id="li-tab-settings-bank"><a href="#tab-settings-bank" data-toggle="tab"><?= $text_settings_bank; ?></a></li>
                            <li class="" id="li-tab-settings-extension"><a href="#tab-settings-extension" data-toggle="tab"><?= $text_settings_extension; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-settings-bank">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_shop_id"><?= $entry_shop_id; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_shop_id" value="<?= $avangard_shop_id; ?>" placeholder="<?= $entry_shop_id; ?>" id="avangard_shop_id" class="form-control" />
                                        <?php if ($error_shop_id) { ?>
                                            <div class="text-danger"><?= $error_shop_id; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_shop_sign"><?= $entry_shop_sign; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_shop_sign" value="<?= $avangard_shop_sign; ?>" placeholder="<?= $entry_shop_sign; ?>" id="avangard_shop_sign" class="form-control" />
                                        <?php if ($error_shop_sign) { ?>
                                            <div class="text-danger"><?= $error_shop_sign; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_av_sign"><?= $entry_av_sign; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_av_sign" value="<?= $avangard_av_sign; ?>" placeholder="<?= $entry_av_sign; ?>" id="avangard_av_sign" class="form-control" />
                                        <?php if ($error_av_sign) { ?>
                                            <div class="text-danger"><?= $error_av_sign; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_pwd"><?= $entry_pwd; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_pwd" value="<?= $avangard_pwd; ?>" placeholder="<?= $entry_pwd; ?>" id="avangard_pwd" class="form-control" />
                                        <?php if ($error_pwd) { ?>
                                            <div class="text-danger"><?= $error_pwd; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_callback_url"><span data-toggle="tooltip" title="<?= $help_callback_url; ?>"><?= $entry_callback_url; ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            <input type="text" name="avangard_callback_url" readonly value="<?= $default_callback_url; ?>" id="avangard_callback_url" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_url_pay"><span data-toggle="tooltip" title="<?= $help_url_pay; ?>"><?= $entry_url_pay; ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            <input type="text" name="avangard_url_pay" value="<?= (isset($avangard_url_pay)) ? $avangard_url_pay : $default_url_pay; ?>" placeholder="<?= $help_url_pay; ?>" id="avangard_url_pay" class="form-control" />
                                        </div>
                                        <?php if ($error_url_pay) { ?>
                                            <div class="text-danger"><?= $error_url_pay; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_url_get_order_info"><span data-toggle="tooltip" title="<?= $help_url_get_order_info; ?>"><?= $entry_url_get_order_info; ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            <input type="text" name="avangard_url_get_order_info" value="<?= (isset($avangard_url_get_order_info)) ? $avangard_url_get_order_info : $default_url_get_order_info; ?>" placeholder="<?= $help_url_get_order_info; ?>" id="avangard_url_get_order_info" class="form-control" />
                                        </div>
                                        <?php if ($error_url_get_order_info) { ?>
                                            <div class="text-danger"><?= $error_url_get_order_info; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_url_reg"><span data-toggle="tooltip" title="<?= $help_url_reg; ?>"><?= $entry_url_reg; ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            <input type="text" name="avangard_url_reg" value="<?= (isset($avangard_url_reg)) ? $avangard_url_reg : $default_url_reg; ?>" placeholder="<?= $help_url_reg; ?>" id="avangard_url_reg" class="form-control" />
                                        </div>
                                        <?php if ($error_url_reg) { ?>
                                            <div class="text-danger"><?= $error_url_reg; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="avangard_offer_url"><span data-toggle="tooltip" title="<?= $help_offer_url; ?>"><?= $entry_offer_url; ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>
                                            <input type="text" name="avangard_offer_url" value="<?= (isset($avangard_offer_url)) ? $avangard_offer_url : $default_offer_url; ?>" placeholder="<?= $help_offer_url; ?>" id="avangard_offer_url" class="form-control" />
                                        </div>
                                        <?php if ($error_offer_url) { ?>
                                            <div class="text-danger"><?= $error_offer_url; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-settings-extension">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="avangard_total"><span data-toggle="tooltip" title="<?= $help_total; ?>"><?= $entry_total; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_total" value="<?= $avangard_total; ?>" placeholder="<?= $help_total; ?>" id="avangard_total" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="avangard_order_status_id"><?= $entry_order_status; ?></label>
                                    <div class="col-sm-10">
                                        <select name="avangard_order_status_id" id="avangard_order_status" class="form-control">
                                            <?php foreach ($order_statuses as $order_status) { ?>
                                                <?php if ($order_status['order_status_id'] == $avangard_order_status_id) { ?>
                                                    <option value="<?= $order_status['order_status_id']; ?>" selected="selected"><?= $order_status['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?= $order_status['order_status_id']; ?>"><?= $order_status['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="avangard_geo_zone"><?= $entry_geo_zone; ?></label>
                                    <div class="col-sm-10">
                                        <select name="avangard_geo_zone" id="avangard_geo_zone" class="form-control">
                                            <option value="0"><?= $text_all_zones; ?></option>
                                            <?php foreach ($geo_zones as $geo_zone) { ?>
                                                <?php if ($geo_zone['geo_zone_id'] == $avangard_geo_zone) { ?>
                                                    <option value="<?= $geo_zone['geo_zone_id']; ?>" selected="selected"><?= $geo_zone['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?= $geo_zone['geo_zone_id']; ?>"><?= $geo_zone['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="avangard_status"><?= $entry_status; ?></label>
                                    <div class="col-sm-10">
                                        <select name="avangard_status" id="avangard_status" class="form-control">
                                            <?php if ($avangard_status) { ?>
                                                <option value="1" selected="selected"><?= $text_enabled; ?></option>
                                                <option value="0"><?= $text_disabled; ?></option>
                                            <?php } else { ?>
                                                <option value="1"><?= $text_enabled; ?></option>
                                                <option value="0" selected="selected"><?= $text_disabled; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="avangard_sort_order"><?= $entry_sort_order; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avangard_sort_order" value="<?= $avangard_sort_order; ?>" placeholder="<?= $entry_sort_order; ?>" id="avangard_sort_order" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?= $footer; ?>