<?php
/**
 * Class ControllerPaymentAvangard
 *
 * @version 1.0
 * @author Alex Zakharov <alexzakharovwork@gmail.com>
 * @copyright (c) 2017-2018 ARTWEBIT Team <it@artwebit.ru>
 */
class ControllerExtensionPaymentAvangard extends Controller {
    /** @var array $error */
    private $error = [];

    /**
     * This method index
     */
    public function index() {
        /** Avangard text will be transferred to the template view files. */
        $this->load->language('extension/payment/avangard');
        // Set title of page
        $this->document->setTitle($this->language->get('heading_title'));
        // Load default model for settings
        $this->load->model('setting/setting');
        /**
         *  When the form is submitted through POST method and the validate function of the controller returns true,
         *  all the changes are saved and success message is assigned to success variable and will redirected to the module listing page.
         */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('avangard', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
        }

        // Heading
        $data['heading_title'] = $this->language->get('heading_title');

        // Text
        $data['text_edit']                  = $this->language->get('text_edit');
        $data['text_enabled']               = $this->language->get('text_enabled');
        $data['text_disabled']              = $this->language->get('text_disabled');
        $data['text_all_zones']             = $this->language->get('text_all_zones');
        $data['text_settings_bank']         = $this->language->get('text_settings_bank');
        $data['text_settings_extension']    = $this->language->get('text_settings_extension');

        // Entry
        $data['entry_shop_id']            = $this->language->get('entry_shop_id');
        $data['entry_shop_sign']          = $this->language->get('entry_shop_sign');
        $data['entry_pwd']                = $this->language->get('entry_pwd');
        $data['entry_av_sign']            = $this->language->get('entry_av_sign');
        $data['entry_order_status']       = $this->language->get('entry_order_status');
        $data['entry_total']              = $this->language->get('entry_total');
        $data['entry_geo_zone']           = $this->language->get('entry_geo_zone');
        $data['entry_status']             = $this->language->get('entry_status');
        $data['entry_sort_order']         = $this->language->get('entry_sort_order');
        $data['entry_url_reg']            = $this->language->get('entry_url_reg');
        $data['entry_url_pay']            = $this->language->get('entry_url_pay');
        $data['entry_url_get_order_info'] = $this->language->get('entry_url_get_order_info');
        $data['entry_callback_url']       = $this->language->get('entry_callback_url');
        $data['entry_offer_url']          = $this->language->get('entry_offer_url');

        // Help
        $data['help_total']              = $this->language->get('help_total');
        $data['help_url_reg']            = $this->language->get('help_url_reg');
        $data['help_url_pay']            = $this->language->get('help_url_pay');
        $data['help_url_get_order_info'] = $this->language->get('help_url_get_order_info');
        $data['help_callback_url']       = $this->language->get('help_callback_url');
        $data['help_offer_url']          = $this->language->get('help_offer_url');

        // Default
        $data['default_url_reg']            = $this->language->get('default_url_reg');
        $data['default_url_pay']            = $this->language->get('default_url_pay');
        $data['default_url_get_order_info'] = $this->language->get('default_url_get_order_info');
        $data['default_callback_url']       = $this->language->get('default_callback_url');
        $data['default_offer_url']          = $this->language->get('default_offer_url');

        // Buttons
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        // check Warnings
        $data['error_warning'] = (isset($this->error['warning'])) ? $this->error['warning'] : '';
        //check isset error shop_id
        $data['error_shop_id'] = (isset($this->error['shop_id'])) ? $this->error['shop_id'] : '';
        //check isset error shop_sign
        $data['error_shop_sign'] = (isset($this->error['shop_sign'])) ? $this->error['shop_sign'] : '';
        //check isset error callback_url
        $data['error_callback_url'] = (isset($this->error['callback_url'])) ? $this->error['callback_url'] : '';
        //check isset error pwd
        $data['error_pwd'] = (isset($this->error['pwd'])) ? $this->error['pwd'] : '';
        //check isset error av_sign
        $data['error_av_sign'] = (isset($this->error['av_sign'])) ? $this->error['av_sign'] : '';
        //check isset error url_reg
        $data['error_url_reg'] = (isset($this->error['url_reg'])) ? $this->error['url_reg'] : '';
        //check isset error url_pay
        $data['error_url_pay'] = (isset($this->error['url_pay'])) ? $this->error['url_pay'] : '';
        //check isset error url_get_order_info
        $data['error_url_get_order_info'] = (isset($this->error['url_get_order_info'])) ? $this->error['url_get_order_info'] : '';
        //check isset error offer_url
        $data['error_offer_url'] = (isset($this->error['offer_url'])) ? $this->error['offer_url'] : '';

        // initial array breadcrumbs
        $data['breadcrumbs'] = [];
        // Add firs elem redirect to home page
        $data['breadcrumbs'][] =
            [
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
            ];
        // add second elem redirect to extensions payments
        $data['breadcrumbs'][] =
            [
                'text'      => $this->language->get('text_extension'),
                'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
            ];
        // add last elem current location
        $data['breadcrumbs'][] =
            [
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('extension/payment/avangard', 'token=' . $this->session->data['token'], true)
            ];

        // link when save
        $data['action'] = $this->url->link('extension/payment/avangard', 'token=' . $this->session->data['token'], true);
        // link when cancel
        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);

        // avangard_shop_id
        $data['avangard_shop_id'] = (isset($this->request->post['avangard_shop_id'])) ? $this->request->post['avangard_shop_id'] : $this->config->get('avangard_shop_id');
        // avangard_shop_sign
        $data['avangard_shop_sign'] = (isset($this->request->post['avangard_shop_sign'])) ? $this->request->post['avangard_shop_sign'] : $this->config->get('avangard_shop_sign');
        // avangard_callback_url
        $data['avangard_callback_url'] = (isset($this->request->post['avangard_callback_url'])) ? $this->request->post['avangard_callback_url'] : $this->config->get('avangard_callback_url');
        // avangard_pwd
        $data['avangard_pwd'] = (isset($this->request->post['avangard_pwd'])) ? $this->request->post['avangard_pwd'] : $this->config->get('avangard_pwd');
        // avangard_av_sign
        $data['avangard_av_sign'] = (isset($this->request->post['avangard_av_sign'])) ? $this->request->post['avangard_av_sign'] : $this->config->get('avangard_av_sign');
        // avangard_total
        $data['avangard_total'] = (isset($this->request->post['avangard_total'])) ? $this->request->post['avangard_total'] : $this->config->get('avangard_total');
        // avangard_status
        $data['avangard_status'] = (isset($this->request->post['avangard_status'])) ? $this->request->post['avangard_status'] : $this->config->get('avangard_status');
        // avangard_sort_order
        $data['avangard_sort_order'] = (isset($this->request->post['avangard_sort_order'])) ? $this->request->post['avangard_sort_order'] : $this->config->get('avangard_sort_order');
        // avangard_order_status
        $data['avangard_order_status_id'] = (isset($this->request->post['avangard_order_status_id'])) ? $this->request->post['avangard_order_status_id'] : $this->config->get('avangard_order_status_id');
        // avangard_url_reg
        $data['avangard_url_reg'] = (isset($this->request->post['avangard_url_reg'])) ? $this->request->post['avangard_url_reg'] : $this->config->get('avangard_url_reg');
        // avangard_url_pay
        $data['avangard_url_pay'] = (isset($this->request->post['avangard_url_pay'])) ? $this->request->post['avangard_url_pay'] : $this->config->get('avangard_url_pay');
        // avangard_url_get_order_info
        $data['avangard_url_get_order_info'] = (isset($this->request->post['avangard_url_get_order_info'])) ? $this->request->post['avangard_url_get_order_info'] : $this->config->get('avangard_url_get_order_info');
        // avangard_offer_url
        $data['avangard_offer_url'] = (isset($this->request->post['avangard_offer_url'])) ? $this->request->post['avangard_offer_url'] : $this->config->get('avangard_offer_url');

        $this->load->model('localisation/order_status');
        // order_statuses
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        // avangard_geo_zone
        $data['avangard_geo_zone'] = (isset($this->request->post['avangard_geo_zone'])) ? $this->request->post['avangard_geo_zone'] : $this->config->get('avangard_geo_zone');

        $this->load->model('localisation/geo_zone');
        // geo_zones
        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // render form
        $this->response->setOutput($this->load->view('extension/payment/avangard', $data));
    }

    /**
     * This method validate form
     * @return bool
     */
    private function validate() {
        // check permission to modify settings
        if (!$this->user->hasPermission('modify', 'extension/payment/avangard')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['avangard_shop_id']) {
            $this->error['shop_id'] = $this->language->get('error_shop_id');
        }
        if (!$this->request->post['avangard_pwd']) {
            $this->error['pwd'] = $this->language->get('error_pwd');
        }
        if (!$this->request->post['avangard_shop_sign']) {
            $this->error['shop_sign'] = $this->language->get('error_shop_sign');
        }
        if (!$this->request->post['avangard_av_sign']) {
            $this->error['av_sign'] = $this->language->get('error_av_sign');
        }
        if (!$this->request->post['avangard_url_reg']) {
            $this->error['url_reg'] = $this->language->get('error_url_reg');
        }
        if (!$this->request->post['avangard_url_pay']) {
            $this->error['url_pay'] = $this->language->get('error_url_pay');
        }
        if (!$this->request->post['avangard_url_get_order_info']) {
            $this->error['url_get_order_info'] = $this->language->get('error_url_get_order_info');
        }
        if (!$this->request->post['avangard_offer_url']) {
            $this->error['url_offer_url'] = $this->language->get('error_offer_url');
        }
        return !$this->error;
    }
}
