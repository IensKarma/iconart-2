<?php
// Heading
$_['heading_title']            = 'Оплата через Банк Авангард';

// Text
$_['text_avangard']            = '<a href="https://www.avangard.ru/rus/" target="_blank"><img src="view/image/payment/avangard-logo.png"></a>';
$_['text_extension']           = 'Расширения';
$_['text_success']             = 'Настройки модуля обновлены!';
$_['text_edit']                = 'Редактирование';
$_['text_settings_bank']       = 'Настройки банка';
$_['text_settings_extension']  = 'Настройки модуля';

// Entry
$_['entry_shop_id']              = 'ID Вашего магазина (SHOP_ID):';
$_['entry_shop_sign']            = 'Название Вашего магазина (SHOP_NAME SHOP_SIGN):';
$_['entry_pwd']                  = 'Пароль Вашего магазина (PWD):';
$_['entry_av_sign']              = 'Авангард сигнатура (AV_SIGN):';
$_['entry_order_status']         = 'Статус заказа после оплаты:';
$_['entry_total']		         = 'Нижняя граница:';
$_['entry_geo_zone']             = 'Географическая зона:';
$_['entry_status']               = 'Статус:';
$_['entry_sort_order']           = 'Порядок сортировки:';
$_['entry_url_reg']              = 'URL банка регистрация:';
$_['entry_url_pay']              = 'URL банка платеж:';
$_['entry_url_get_order_info']   = 'URL банка, для получения информации о ордере:';
$_['entry_callback_url']         = 'URL для запросов банка:';
$_['entry_offer_url']            = 'URL оферты:';

// Help
$_['help_total']		         = 'Минимальная сумма заказа. Ниже данной суммы, способ оплаты будет недоступен.';
$_['help_url_reg']               = 'Сверьтесь с документацией предоставленной банком, если не подходит (https://www.avangard.ru/iacq/h2h/reg)';
$_['help_url_pay']               = 'Сверьтесь с документацией предоставленной банком, если не подходит (https://www.avangard.ru/iacq/pay)';
$_['help_url_get_order_info']    = 'Сверьтесь с документацией предоставленной банком, если не подходит (https://www.avangard.ru/iccq/h2h/get_order_info)';
$_['help_callback_url']          = 'Адрес, куда эквайринг будет отправлять запрос, настраивается для Интернет-магазина в индивидуальном порядке. Необходимо предоставить банку.';
$_['help_offer_url']             = 'Адрес, где располагаеться оферта';

// Error
$_['error_permission']           = 'У Вас нет прав для управления этим модулем!';
$_['error_shop_id']              = 'Необходим идентификатор магазина!';
$_['error_shop_sign']            = 'Необходимо название магазина(SHOP_SIGN)!';
$_['error_pwd']                  = 'Необходим пароль!';
$_['error_av_sign']              = 'Необходим идентификатор(AV_SIGN)!';
$_['error_url_reg']              = 'Необходим URL регистрации!';
$_['error_url_pay']              = 'Необходим URL платежа!';
$_['error_url_get_order_info']   = 'Необходим URL для получения информации о ордере!';
$_['error_callback_url']         = 'Неоюходим URL для сервиса уведомления об успешной оплате!';
$_['error_offer_url']            = 'Необходим URL оферты!';

// Default
$_['default_url_reg']            = 'https://www.avangard.ru/iacq/h2h/reg';
$_['default_url_pay']            = 'https://www.avangard.ru/iacq/pay';
$_['default_url_get_order_info'] = 'https://www.avangard.ru/iacq/h2h/get_order_info';
$_['default_callback_url']       = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php?route=extension/payment/avangard/callback';
$_['default_offer_url']          = '#';