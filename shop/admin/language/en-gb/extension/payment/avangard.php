<?php
// Heading
$_['heading_title']            = 'Payment through Avangard Bank';

// Text
$_['text_avangard']            = '<a href="https://www.avangard.ru/rus/" target="_blank"><img src="view/image/payment/avangard-logo.png"></a>';
$_['text_extension']           = 'Extensions';
$_['text_success']             = 'Module settings updated!';
$_['text_edit']                = 'Edit';
$_['text_settings_bank']       = 'Settings of Bank';
$_['text_settings_extension']  = 'Settings of Extension';

// Entry
$_['entry_shop_id']            = 'Your store ID (SHOP_ID):';
$_['entry_shop_sign']          = 'Name of your store (SHOP_NAME, SHOP_SIGN):';
$_['entry_pwd']                = 'Your shop password (PWD):';
$_['entry_av_sign']            = '(AV_SIGN):';
$_['entry_total']		       = 'Bottom line:';
$_['entry_order_status']       = 'Order status after payment:';
$_['entry_geo_zone']           = 'Geographical area:';
$_['entry_status']             = 'Status:';
$_['entry_sort_order']         = 'Sorting order:';
$_['entry_url_reg']            = 'Bank URL registration:';
$_['entry_url_pay']            = 'Bank URL payment:';
$_['entry_url_get_order_info'] = 'URL of the bank, for information about the order:';
$_['entry_callback_url']       = 'URL Callback:';
$_['entry_offer_url']          = 'URL offer:';

// Help
$_['help_total']		       = 'The minimum order amount. Below this amount, the method will be unavailable.';
$_['help_url_reg']             = 'Check with the documentation provided by the bank, if not suitable (https://www.avangard.ru/iacq/h2h/reg)';
$_['help_url_pay']             = 'Check with the documentation provided by the bank, if not suitable (https://www.avangard.ru/iacq/pay)';
$_['help_url_get_order_info']  = 'Check with the documentation provided by the bank, if not suitable (https://www.avangard.ru/iacq/h2h/get_order_info)';
$_['help_callback_url']        = 'The address where the acquiring will send the request is configured for the online store individually. It is necessary to give the bank.';
$_['help_offer_url']           = 'The address where the offer is located';

// Error
$_['error_permission']         = 'You do not have the rights to manage this module!';
$_['error_shop_id']            = 'Need a store ID!';
$_['error_shop_sign']          = 'Required ID(SHOP_SIGN)!';
$_['error_pwd']                = 'Password required!';
$_['error_av_sign']            = 'Required ID(AV_SIGN)!';
$_['error_url_reg']            = 'Registration URL is required!';
$_['error_url_pay']            = 'Payment URL required!';
$_['error_url_get_order_info'] = 'Requires a URL to retrieve information about the order!';
$_['error_callback_url']       = 'Requires a URL to indicate the payment for a successful payment!';
$_['error_offer_url']          = 'Required URL offer!';

// Default
$_['default_url_reg']            = 'https://www.avangard.ru/iacq/h2h/reg';
$_['default_url_pay']            = 'https://www.avangard.ru/iacq/pay';
$_['default_url_get_order_info'] = 'https://www.avangard.ru/iacq/h2h/get_order_info';
$_['default_callback_url']       = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php?route=extension/payment/avangard/callback';
$_['default_offer_url']          = '#';

